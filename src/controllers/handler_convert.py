import math
from turtle import pos
from PIL import Image
from flask import jsonify
import os


def calcSize(oldSize, newWidth):
    oldWidth, oldHeight = oldSize
    newHeight = (newWidth/oldWidth)*oldHeight
    return (int(round(newWidth)), int(round(newHeight)))


def handlerConvert(post:dict):
    res = {}

    path = post['path']
    print(path)

    if(not os.path.exists(path)):
        res['status'] = 'failure'
        res['message'] = 'path not exists!'
        return jsonify(res), 400

    actions = post['actions']

    img = Image.open(path)

    gcd = math.gcd(img.size[0], img.size[1])
    ratio = (
        round(img.size[0]/gcd),
        round(img.size[1]/gcd)
    )
    strRatio = f'{ratio[0]}x{ratio[1]}'

    res['size'] = {
        'width': img.size[0],
        'height': img.size[1]
    }

    res['data'] = []

    for action in actions:
        pathForSave = str(action['save'])
        width = action['width']
        lossless = action['lossless']
        quality = int(action['quality'])
        thumbnail = action['thumbnail']

        imgCopy = img.copy()

        if(thumbnail):
            imgCopy.thumbnail((width, width), Image.ANTIALIAS)
        else:
            imgCopy = imgCopy.resize(calcSize(imgCopy.size, width), Image.ANTIALIAS)


        

        if(not pathForSave.endswith('.webp')):
            pathForSave = pathForSave + '_' + strRatio + '.webp'

        imgCopy.save(
            pathForSave, 
            'webp',
            method=6,
            lossless=lossless,
            quality=quality
        )

        res['data'].append({
            'size': {
                'width': imgCopy.size[0],
                'height': imgCopy.size[1]
            },
            'path': pathForSave
        })
        print(pathForSave)
    #endFor


    res['status'] = 'successfully'
    res['message'] = f''
    return jsonify(res), 201