# from urllib import response

from flask import Flask, jsonify, request
import flask
import waitress
import os
import config
from controllers.handler_convert import handlerConvert



PORT = (os.environ.get('PORT') and int(os.environ.get('PORT'))) or 3003

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST', 'PATCH', 'PUT'])
def home():
    return flask.make_response(jsonify({
        'status': 'OK' 
    }), 200)

@app.route('/convert', methods=['POST', 'GET'])
def convertRouter():
    return handlerConvert(request.get_json())


if __name__ == '__main__':
    print(f'FLASK_ENV="{os.environ.get("FLASK_ENV")}"')

    if(os.environ.get('FLASK_ENV') == 'production'):
        waitress.serve(app, host='0.0.0.0', port=PORT)
    else:
        app.run(host='0.0.0.0', port=PORT, debug=True)

    print(f'Server running on PORT={PORT}')
